package com.zuitt.activity;

public class Contact {

    // completeName
    private String completeName;

    public void setCompleteName(String newCompleteName){
        this.completeName = newCompleteName;
    }
    public String getCompleteName(){
        return this.completeName;
    }


    // contactNumber1
    private String contactNumber1;
    public void setContactNumber1(String newContactNumber1){
        this.contactNumber1 = newContactNumber1;
    }
    public String getContactNumber1(){
        return this.contactNumber1;
    }

    // homeAddress
    private String homeAddress;
    public void setHomeAddress(String newHomeAddress){
        this.homeAddress = newHomeAddress;
    }
    public String getHomeAddress(){
        return this.homeAddress;
    }

    // officeAddress
    private String officeAddress;
    public void setOfficeAddress(String newOfficeAddress){
        this.officeAddress = newOfficeAddress;
    }
    public String getOfficeAddress(){
        return this.officeAddress;
    }

    public Contact(){

    }

    public Contact(String completeName, String contactNumber1, String homeAddress, String officeAddress){
        this.completeName = completeName;
        this.contactNumber1 = contactNumber1;
        this.homeAddress = homeAddress;
        this.officeAddress = officeAddress;
    }


}
