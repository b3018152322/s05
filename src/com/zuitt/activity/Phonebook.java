package com.zuitt.activity;

import java.util.ArrayList;

public class Phonebook {

    // composition
    private Contact c;

    private ArrayList<String> contacts;

    // default constructor
    public Phonebook(){
        contacts = new ArrayList<>();
    }

    // parameterized constructor
    public Phonebook(ArrayList<String> newArray){
        contacts = newArray;
    }

    // setter
    public void addCompleteName(String completeName){
        contacts.add(completeName);
    }
    public void addContactNumber(String contactNumber){
        contacts.add(contactNumber);
    }

    public void addHomeAddress(String homeAddress){
        contacts.add(homeAddress);
    }

    public void addOfficeAddress(String officeAddress){
        contacts.add(officeAddress);
    }

    // getter
    public ArrayList<String> getContacts(){
        return contacts;
    }

}
