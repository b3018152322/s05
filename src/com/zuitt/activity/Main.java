package com.zuitt.activity;

import java.util.Objects;

public class Main {
    public static void main(String[] args){
        Contact contact1 = new Contact();

        contact1.setCompleteName("John Doe");
        contact1.setContactNumber1("9152468596");
        contact1.setHomeAddress("Quezon City");
        contact1.setOfficeAddress("Makati City");

        boolean a = !Objects.equals(contact1.getCompleteName(), "");
        boolean b = !Objects.equals(contact1.getContactNumber1(), "");
        boolean c = !Objects.equals(contact1.getHomeAddress(), "");
        boolean d = !Objects.equals(contact1.getOfficeAddress(), "");


        if(a == true && b == true &&  c == true && d == true){
            // Entries for Phonebook

                Phonebook phonebook1 = new Phonebook();
                phonebook1.addCompleteName(contact1.getCompleteName());
                phonebook1.addContactNumber("+63" + contact1.getContactNumber1());
                phonebook1.addHomeAddress(contact1.getHomeAddress());
                phonebook1.addOfficeAddress(contact1.getOfficeAddress());

            for(String name: phonebook1.getContacts()){
                //Output
                System.out.println("=========================================================================================");
                System.out.println(phonebook1.getContacts().get(0));
                System.out.println("-----------------------------------------------------------------------------------------");
                System.out.println(phonebook1.getContacts().get(0) + " has the following registered number: ");
                System.out.println(phonebook1.getContacts().get(1));
                System.out.println("-----------------------------------------------------------------------------------------");
                System.out.println(phonebook1.getContacts().get(0) + " has the following registered addresses: ");
                System.out.println("my home is in " + phonebook1.getContacts().get(2));
                System.out.println("my office is in " + phonebook1.getContacts().get(3));
            }


        }
        else{
            System.out.println("You have missing entry/entries. Try again!");
        }
    }
}
